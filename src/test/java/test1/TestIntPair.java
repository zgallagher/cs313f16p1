package test1;
import static org.junit.Assert.*;


import org.junit.Test;

public class TestIntPair {
    IntPair p = new DefaultIntPair(3,5);
    IntPair q= new DefaultIntPair(5,3);





    @Test
    public void testEquality() {
        assertEquals(3, p.first());
        assertEquals(5, p.second());
        assertEquals("<3,5>", p.toString());


    }
    @Test
    public void testTrue(){
        assertTrue(p.equals(p));
        assertFalse(p.equals(q));
        assertFalse(q.equals(p));
        assertFalse(p.equals(null));

    }
    @Test
    public void testReversal(){
        System.out.println(p);
        System.out.println(q.reverse());
        assertTrue(p.equals(q.reverse()));//I don't know why this is not coming out correctly

    }
}

