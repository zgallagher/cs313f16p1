package test1;

import java.util.ArrayList;

/**
 * Created by zac gallagher on 10/14/2016.
 */

public class DefaultIntPair implements IntPair {
    private int first = 0;
    private int second =0;
    DefaultIntPair(int f, int s){

        this.first =f;
        this.second=s;


    }
    @Override
    public int first() {
        return first;
    }

    @Override
    public int second() {
        return second;
    }

    @Override
    public IntPair reverse() {
    IntPair reversed = new DefaultIntPair(second,first);
        return reversed;
    }

    public String toString(){
        String newString;
        newString ="<"+Integer.toString(first)+","+Integer.toString(second)+">";
        return newString;
    }
}
