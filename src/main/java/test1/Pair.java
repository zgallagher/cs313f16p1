package test1;

/**
 * Created by zgdel on 10/14/2016.
 */

public interface Pair<X,Y> {
   public void first(X object);
   public void second(Y object);
   public void reverse(X object1 ,Y object);


}
