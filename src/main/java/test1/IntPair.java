package test1;

import java.util.ArrayList;

/**
 * Created by zgdel on 10/14/2016.
 */

public interface IntPair {
    int first();
    int second();
    IntPair reverse();


}


